import { Row, Col, Card } from "react-bootstrap";
import { Carousel } from "react-bootstrap";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";
import { useState } from "react";
import { Link } from "react-router-dom";

export default function Highlights() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();

    Swal.fire({
      icon: "success",
      title: "Thank you!",
      text: "Your message has been sent.",
    });
    // Reset form inputs
    setName("");
    setEmail("");
    setMessage("");
  };
  return (
    <>
      <section className="carouselSection">
        <Carousel className="carouselHighlights">
          <Carousel.Item interval={700}>
            <img
              className="d-block w-100"
              src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/REDMAGIC-8-Pro-PDP-Camera-PC__2x_5a0429ed-01f3-4f35-b901-d66111365e48.jpg?v=1673840408"
              alt="First slide"
              style={{ height: "500px", objectFit: "cover" }}
            />
            <Carousel.Caption>
              <h1 className="carousel1">"50 MP Triple Lens Camera"</h1>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={700}>
            <img
              className="d-block w-100"
              src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/REDMAGIC-8-Pro-PDP-960hz-PC_2x_25e17f11-5647-4625-8ed1-935b0e1424d9.jpg?v=1673840330"
              alt="Second slide"
              style={{ height: "500px", objectFit: "cover" }}
            />
            <Carousel.Caption>
              <h1 className="carousel2">
              960 Hz Touch Sampling Rate.
              </h1>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/REDMAGIC-8-Pro-PDP-Studio-PC_2x_4f127f58-d3d5-476d-b348-f06016cb0aa0.jpg?v=1673840366"
              alt="Third slide"
              style={{ height: "500px", objectFit: "cover" }}
            />
            <Carousel.Caption>
              <h3 className="carousel3">RGB Lights</h3>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </section>

      <section className="categorySection">
        <h2 className="categoryTxt">CATEGORIES</h2>
        <Row className="mt-3 mb-3">
          <Col xs={12} md={4}>
            <Link to="/products/getAllActiveProducts" className="card-link">
              <Card className="cardHighlight p-3">
                <Card.Body>
                  <Card.Title>
                    <h2>PHONES</h2>
                  </Card.Title>
                  <img
                    className="d-block w-100"
                    src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/REDMAGIC-Gaming-Smartphones-Navi.png?v=1675309733"
                    alt="Third slide"
                    style={{
                      height: "400px",
                      objectFit: "cover",
                      borderRadius: "10px",
                    }}
                  />
                </Card.Body>
              </Card>
            </Link>
          </Col>
          <Col xs={12} md={4}>
            <Link to="/products/getAllActiveProducts" className="card-link">
              <Card className="cardHighlight p-3">
                <Card.Body>
                  <Card.Title>
                    <h2>ACCESORIES</h2>
                  </Card.Title>
                  <img
                    className="d-block w-100"
                    src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/Accessories_30ccc9fd-ba82-4383-ae27-92ba38787133.png?v=1672929636"
                    alt="Third slide"
                    style={{
                      height: "400px",
                      objectFit: "cover",
                      borderRadius: "10px",
                    }}
                  />
                </Card.Body>
              </Card>
            </Link>
          </Col>
          <Col xs={12} md={4}>
            <Link to="/products/getAllActiveProducts" className="card-link">
              <Card className="cardHighlight p-3">
                <Card.Body>
                  <Card.Title>
                    <h2>BUNDLES</h2>
                  </Card.Title>
                  <img
                    className="d-block w-100"
                    src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/Bundle_bbc9e417-b2b7-4235-bd58-4143e2ce2831.png?v=1672929636"
                    alt="Third slide"
                    style={{
                      height: "400px",
                      objectFit: "cover",
                      borderRadius: "10px",
                    }}
                  />
                </Card.Body>
              </Card>
            </Link>
          </Col>
        </Row>
      </section>
      <section className="contactSection">
        <Row className="mt-3 mb-3">
          <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
              <Card.Body>
                <Card.Title>
                  <h2>Leave Message Here</h2>
                </Card.Title>
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="formBasicName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicMessage">
                    <Form.Label>Message</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      placeholder="Enter your message here"
                      value={message}
                      onChange={(e) => setMessage(e.target.value)}
                    />
                  </Form.Group>

                  <Button
                    variant="primary"
                    type="submit"
                    className="contactBtn"
                  >
                    Submit
                  </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12} md={8}>
            <Card className="contactPicCard">
              <Card.Img
                variant="top"
                className="contactPic"
                src="https://cdn.shopify.com/s/files/1/0024/0684/2441/files/tile_3_2x_7a514941-95f4-4a19-ad6b-2d9f1932f07b.jpg?v=1672929646&width=1920"
                style={{
                  height: "450px",
                  width: "100%",
                  objectFit: "cover",
                }}
              />
            </Card>
          </Col>
        </Row>
      </section>
    </>
  );
}
