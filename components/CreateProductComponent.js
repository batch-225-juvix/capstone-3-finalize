// import React, { useState } from "react";

// export default function CreateProductComponent() {
//   const [productName, setProductName] = useState("");
//   const [productDescription, setProductDescription] = useState("");
//   const [price, setPrice] = useState("");
//   const [productImage, setProductImage] = useState("upload");
//   const [imageUrl, setImageUrl] = useState("");
//   const [imageFile, setImageFile] = useState();

//   function handleSubmit(event) {
//     event.preventDefault();

//     const formData = new FormData();
//     formData.append("productName", productName);
//     formData.append("productDescription", productDescription);
//     formData.append("price", price);
//     formData.append("isActive", true);
//     formData.append("createdOn", new Date());

//     if (productImage === "link") {
//       formData.append("productImage", imageUrl);
//     } else {
//       formData.append("productImage", imageFile);
//     }

//     // const options = {
//     //   method: "POST",
//     //   body: formData,
//     // };
//     const data = {
//       isAdmin: true,
//       product: {
//         productName,
//         productDescription,
//         price,
//         productImage,
//         createdOn: new Date(),
//       },
//     };

//     fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
//       method: "POST",
//       //   headers: {
//       //     "Content-Type": "application/json",
//       //   },
//       body: JSON.stringify(data),
//     })
//       .then((response) => response.json())
//       .then((result) => {
//         console.log(JSON.stringify(result));
//         alert(JSON.stringify(result));
//       })
//       .catch((error) => {
//         console.error("Error:", error);
//       })
//       .then((response) => response.json())
//       .then((result) => {
//         alert(result);
//       })
//       .catch((error) => {
//         console.error("Error:", error);
//       });
//   }

//   function handleImageUpload(event) {
//     const file = event.target.files[0];
//     setImageFile(file);
//     setProductImage("upload");
//   }

//   function handleImageLink(event) {
//     setProductImage("link");
//   }

//   function handleImageUrlChange(event) {
//     setImageUrl(event.target.value);
//   }

//   return (
//     <form onSubmit={handleSubmit}>
//       <label>
//         Product Name:
//         <input
//           type="text"
//           value={productName}
//           onChange={(event) => setProductName(event.target.value)}
//         />
//       </label>
//       <br />
//       <label>
//         Product Description:
//         <textarea
//           value={productDescription}
//           onChange={(event) => setProductDescription(event.target.value)}
//         />
//       </label>
//       <br />
//       <label>
//         Price:
//         <input
//           type="number"
//           value={price}
//           onChange={(event) => setPrice(event.target.value)}
//         />
//       </label>
//       <br />
//       <div>
//         <label>
//           Image Source:
//           <input
//             type="radio"
//             name="imageSource"
//             value="link"
//             checked={productImage === "link"}
//             onChange={handleImageLink}
//           />
//           Link
//         </label>
//         <label>
//           <input
//             type="radio"
//             name="imageSource"
//             value="upload"
//             checked={productImage === "upload"}
//             onChange={() => setProductImage("upload")}
//           />
//           Upload
//           <input type="file" onChange={handleImageUpload} accept="image/*" />
//         </label>
//       </div>
//       {productImage === "link" && (
//         <div>
//           <label>
//             Image URL:
//             <input
//               type="text"
//               value={imageUrl}
//               onChange={handleImageUrlChange}
//             />
//           </label>
//         </div>
//       )}
//       <br />
//       <button type="submit">Create Product</button>
//     </form>
//   );
// }

// import React, { useState } from "react";
// import { Form, Button } from "react-bootstrap";

// export default function CreateProductComponent({ onSubmit }) {
//   const [productName, setProductName] = useState("");
//   const [productDescription, setProductDescription] = useState("");
//   const [price, setPrice] = useState("");
//   const [productImage, setProductImage] = useState("");
//   const [imageUrl, setImageUrl] = useState("");
//   const [imagePreview, setImagePreview] = useState(null);

//   const handleSubmit = (event) => {
//     event.preventDefault();

//     const formData = new FormData();
//     formData.append("productName", productName);
//     formData.append("productDescription", productDescription);
//     formData.append("price", price);
//     formData.append("isActive", true);
//     formData.append("createdOn", new Date());

//     // if (productImage === "link") {
//     //   formData.append("productImage", imageUrl);
//     // } else {
//     //   formData.append("productImage", event.target.productImage.files[0]);
//     // }

//     fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
//       method: "POST",
//       body: JSON.stringify({ formData }),
//     })
//       .then((response) => response.json())
//       .then((result) => {
//         console.log(JSON.stringify(result));
//         alert(JSON.stringify(result));
//       })
//       .catch((error) => {
//         console.error("Error:", error);
//         alert(error.message);
//       });
//   };
//   const handleImageLink = () => {
//     setProductImage("link");
//   };

//   //   const handleImageUpload = (event) => {
//   //     if (event && event.target && event.target.files && event.target.files[0]) {
//   //       setProductImage("upload");
//   //       const file = event.target.files[0];
//   //       setImagePreview(URL.createObjectURL(file));
//   //     }
//   //   };

//   const handleImageUrlChange = (event) => {
//     setImageUrl(event.target.value);
//   };

//   return (
//     <Form onSubmit={handleSubmit}>
//       <Form.Group controlId="productName">
//         <Form.Label>Product Name</Form.Label>
//         <Form.Control
//           type="text"
//           placeholder="Enter product name"
//           value={productName}
//           onChange={(event) => setProductName(event.target.value)}
//         />
//       </Form.Group>

//       <Form.Group controlId="productDescription">
//         <Form.Label>Product Description</Form.Label>
//         <Form.Control
//           as="textarea"
//           placeholder="Enter product description"
//           value={productDescription}
//           onChange={(event) => setProductDescription(event.target.value)}
//         />
//       </Form.Group>

//       <Form.Group controlId="price">
//         <Form.Label>Price</Form.Label>
//         <Form.Control
//           type="number"
//           placeholder="Enter price"
//           value={price}
//           onChange={(event) => setPrice(event.target.value)}
//         />
//       </Form.Group>

//       <Form.Group controlId="productImage">
//         <Form.Label>Product Image</Form.Label>
//         <div className="mb-3">
//           <Form.Check
//             inline
//             label="Link"
//             type="radio"
//             id="imageLink"
//             checked={productImage === "link"}
//             onChange={handleImageLink}
//           />
//           <Form.Check
//             inline
//             label="Upload"
//             type="radio"
//             id="imageUpload"
//             checked={productImage === "upload"}
//             onChange={() => setProductImage("upload")}
//           />
//         </div>
//         {productImage === "link" && (
//           <Form.Control
//             type="text"
//             placeholder="Enter image URL"
//             value={imageUrl}
//             onChange={handleImageUrlChange}
//           />
//         )}
//         {/* {productImage === "upload" && (
//           <div>
//             <Form.File
//               id="imageUploadInput"
//               label="Choose file"
//               custom
//               onChange={handleImageUpload}
//             />
//             {imagePreview && (
//               <img src={imagePreview} alt="Product" style={{ width: "100%" }} />
//             )}
//           </div>
//         )} */}
//       </Form.Group>

//       <Button variant="primary" type="submit">
//         Create
//       </Button>
//     </Form>
//   );
// }
